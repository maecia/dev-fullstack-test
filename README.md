# Test de recrutement Lead Dev FullStack JS/PHP

Les différentes parties qui vont suivre ont pour but d'évaluer votre aisance avec la technique et les méthodes de développement. 
Certaines questions sont délibérément ouvertes afin d'apprécier votre approche aux problèmes rencontrés.

# PHP
## A. Éléments de code
1. Expliquez 3 utilisations du mot clef "use" en PHP,
2. Un développeur junior a écrit la fonction suivante :
```
 function test($a, $b) {  
     if ($a > $b) {  
	      return 1;  
     }  
     if ($b > $a) {  
	      return -1;  
     }  
     if ($b === $a) {  
	      return UKNOWN_STATE;  
     }
 }
```
Proposez une réécriture succincte de cette fonction.

3. Quelles différences faites-vous entre une classe abstraite et une interface en PHP ?
4. Soit `Dog` une classe PHP. 
	On souhaite pouvoir instancier cette classe en passant un âge, un nom ou une race (un seul argument en entrée).  En proposer une implémentation. 
	Quelles réserves faites-vous sur l'approche initiale ? Quelles méthodes préconiseriez vous ?

## B. Architecture d'un projet PHP
Vous travaillez sur un projet où l'accès aux serveurs de production vous est impossible, pas de back-office ni d'accès SSH. 
**Vous ne pouvez transmettre qu'une liste de commandes à faire exécuter à la DSI de votre client.**
Vous devez passer en production une mise à jour de votre outil faisant intervenir des modifications de **code**, en **base de données** et sur les **fichiers utilisateurs**. 

Proposez une architecture projet et les outils permettant de répondre a cette problématique.


# JAVASCRIPT / HTML / CSS
## A. Éléments de code
Soit le code suivant

```
function createDog (name, breed, age) {
	return {
		name,
		breed,
		age,
		isDog: true,
		bark() {
			return "woof"
		},
		log1: () => {
			console.log(this)
		},
		log2() {
			console.log(this)
		},
		log3: function() {
			console.log(this)
		}
	}
}
   
const margotte = createDog('Margotte', 'cocker', 6)
const salsifi = new createDog('Salsifi, 'golden', 3)
```

1. Expliquez la différence d'implémentation fondamentale entre margotte et salsifi,
2. Quels différences faites vous entre les 3 méthodes de logs, précisez notamment le comportement de la fonction log1,
3. On souhaite assurer que la race (breed) déclarée lors de la création d'une instance via createDog existe et est valide. On veut que les choix de races disponibles soient facilement autocomplétés dans l'IDE des développeurs et qu'ils ne puissent pousser du code utilisant une *breed* non valide. Que proposez-vous ?
4. Afin d'illustrer les 3 points précédents, réaliser une petite application VueJS (ou react) selon la méthode de votre choix,
5. Implémenter un test E2E de votre choix sur cette application.

## B. Organisation des projets JS
Présenter rapidement votre approche pour assurer la qualité du code JS sur vos projets. 
Quelles approches et outils préconisez vous pour la compilation / transpilation de votre code JS.

# COLLABORATION DES ÉQUIPES
Vous travaillez sur un backend *headless* complexe et à forte valeur ajoutée couplé à un frontend en VueJS/react.
Les équipes Javascript (front-end) et PHP (back-end) vont travailler quotidiennement en parallèle sur des fonctionnalités communes en relation avec la chefferie de projet

**Que proposez-vous pour :**

1. Assurer que les discussions autour des nouvelles fonctionnalités soient comprises de tous,
2. Assurer que les API soient implémentées, documentées et utilisées efficacement par tous les développeurs en évitant au maximum les erreurs,
3. Tester et vérifier la non régression des fonctionnalités.
